using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class BoostManager : MonoBehaviour
{
    InGameController inGame;
    SoundManager sound;
    

    // Start is called before the first frame update
    void Start()
    {
        inGame = GameObject.Find("In-Game").GetComponent<InGameController>();
        sound = GameObject.Find("SoundManager").GetComponent<SoundManager>();

        switch (tag)
        {
            case "Boost1":
                GetComponent<Button>().onClick.AddListener(inGame.Boost1Hit);
                GetComponent<Button>().onClick.AddListener(sound.Boost1.Play);
                GetComponent<Button>().onClick.AddListener(AssDestroyer);
                break;
            case "Boost2":
                GetComponent<Button>().onClick.AddListener(inGame.Boost2Hit);
                GetComponent<Button>().onClick.AddListener(sound.Boost2.Play);
                GetComponent<Button>().onClick.AddListener(AssDestroyer);
                break;
            case "Decrease":
                GetComponent<Button>().onClick.AddListener(inGame.DecreaseHit);
                GetComponent<Button>().onClick.AddListener(sound.Malito.Play);
                GetComponent<Button>().onClick.AddListener(AssDestroyer);
                break;
            default:
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
        
    }

    public void AssDestroyer()
    {
        Destroy(gameObject);
    }
}
