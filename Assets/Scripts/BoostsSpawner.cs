using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BoostsSpawner : MonoBehaviour
{
    public GameObject[] interactButtons;


    // Start is called before the first frame update
    void Start()
    {
        Spawn();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Spawn()
    {
        GameObject interactuable = Instantiate(interactButtons[Random.Range(0, 3)], transform.position + Vector3.right * Random.Range(-2.5f, 2.5f), Quaternion.identity);
        interactuable.transform.SetParent(transform);
        
        Invoke("Spawn", 4f);
    }
}
