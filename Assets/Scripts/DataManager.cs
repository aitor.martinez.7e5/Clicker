using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using SimpleJSON;
using System.Text;

public class DataManager : MonoBehaviour
{
    public static DataManager instance;

    public GameObject loadingScreen;
    public Text infoText;

    private GameObject LoginPanel;

    public string nombreLogedPlayer = "";

    public Text UserName;

    void Awake()
    {
        //Creamos un singleton (solo puede existir un RestManager)
        if (DataManager.instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }


    }

    void Start()
    {
        UserName.text = "";
        //infoText = "\n";
        //StartCoroutine(GetAll());
        //StartCoroutine(Register("Dembele", "contrase�a"));
        //StartCoroutine(Login("Dembele", "contrase�a"));
        //StartCoroutine(GetUsuario("usuario1"));
        //StartCoroutine(Actualizar("usuario1", 44));
        //StartCoroutine(Borrar("usuario2"));
    }

    // Update is called once per frame
    /*void Update()
    {
        
    }*/

    public IEnumerator GetAll()
    {
        //Activa la pantalla de carga
        loadingScreen.SetActive(true);
        //infoText.text = "Cargando ...";

        //Hace petici�n de descarga
        UnityWebRequest www = UnityWebRequest.Get("http://localhost:3000/ranking/");
        yield return www.SendWebRequest();
        infoText.text = "";
        //Desactiva la pantalla de carga
        loadingScreen.SetActive(false);

        //En caso de error
        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            infoText.text = "Error : " + www.error;
        }
        else
        {
            // Show results as text
            //Debug.Log(www.downloadHandler.text);

            //Convierte a JSONNode la informaci�n descargada
            JSONNode data = JSON.Parse(www.downloadHandler.text);

            foreach (JSONNode infoCountry in data)
            {
                infoText.text += infoCountry["usuario"] + "   ";
                infoText.text += infoCountry["score"] + "\n";

            }

        }
    }

    public IEnumerator GetUsuario(string usuario)
    {
        loadingScreen.SetActive(true);
        infoText.text = "Cargando ...";
        //La URL de la petici�n depende del par�metro country, por defecto es espa�a pero desde country selector es modificado para cada pa�s seleccionable.
        UnityWebRequest www = UnityWebRequest.Get("http://localhost:3000/ranking/" + usuario);
        yield return www.SendWebRequest();


        loadingScreen.SetActive(false);

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {

            JSONNode data = JSON.Parse(www.downloadHandler.text);

            infoText.text = "Usuario: " + data["usuario"] + "\n";
            infoText.text += "Score: " + data["score"] + "\n";
            //infoText.text += "Contrase�a: " + data["contrase�a"] + "\n";
        }
    }

    public IEnumerator Register(string name, string password)
    {
        string url = "http://localhost:3000/ranking/registro";
        var request = new UnityWebRequest(url, "POST");
        string jsonRequestBody = "{\"usuario\":\"" + name + "\",\"contrase�a\":\"" + password + "\"}";
        byte[] bodyRaw = Encoding.UTF8.GetBytes(jsonRequestBody);
        request.uploadHandler = new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");

        yield return request.SendWebRequest();

        if (request.result != UnityWebRequest.Result.Success)
        {
            Debug.LogError("Error sending request: " + request.error);
        }
        else
        {
            Debug.Log("Register successful!");
            //Server
            nombreLogedPlayer = name;
            //UI
            UserName.text += name;
            //SceneManager.LoadScene(1);
            string responseBody = Encoding.UTF8.GetString(request.downloadHandler.data);
            Debug.Log("Response: " + responseBody);

            LoginPanel = GameObject.Find("Register");
            LoginPanel.SetActive(false);

            LoginPanel = GameObject.Find("Menu");
            LoginPanel.SetActive(false);

            LoginPanel = GameObject.Find("Ranking");
            LoginPanel.SetActive(false);

            LoginPanel = GameObject.Find("RankingTitle");
            LoginPanel.SetActive(false);
        }
    }

    public IEnumerator Login(string name, string password)
    {
        string url = "http://localhost:3000/ranking/login";
        var request = new UnityWebRequest(url, "GET");
        string jsonRequestBody = "{\"usuario\":\"" + name + "\",\"contrase�a\":\"" + password + "\"}";
        byte[] bodyRaw = Encoding.UTF8.GetBytes(jsonRequestBody);
        request.uploadHandler = new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");

        yield return request.SendWebRequest();

        if (request.result != UnityWebRequest.Result.Success)
        {
            Debug.LogError("Error sending request: " + request.error);
        }
        else
        {
            Debug.Log("Login successful!");
            //Server
            nombreLogedPlayer = name;
            //UI
            UserName.text += name;
            //SceneManager.LoadScene(1);
            string responseBody = Encoding.UTF8.GetString(request.downloadHandler.data);
            Debug.Log("Response: " + responseBody);

            LoginPanel = GameObject.Find("Login");
            LoginPanel.SetActive(false);

            LoginPanel = GameObject.Find("Menu");
            LoginPanel.SetActive(false);

            LoginPanel = GameObject.Find("Ranking");
            LoginPanel.SetActive(false);

            LoginPanel = GameObject.Find("RankingTitle");
            LoginPanel.SetActive(false);
        }
    }

    public IEnumerator Actualizar(string name, int score)
    {
        string url = "http://localhost:3000/ranking/actualizarScore";
        var request = new UnityWebRequest(url, "PUT");
        string jsonRequestBody = "{\"usuario\":\"" + name + "\",\"score\":\"" + score + "\"}";
        byte[] bodyRaw = Encoding.UTF8.GetBytes(jsonRequestBody);
        request.uploadHandler = new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");

        yield return request.SendWebRequest();

        if (request.result != UnityWebRequest.Result.Success)
        {
            Debug.LogError("Error sending request: " + request.error);
        }
        else
        {
            Debug.Log("Update successful!");
            //SceneManager.LoadScene(1);
            string responseBody = Encoding.UTF8.GetString(request.downloadHandler.data);
            Debug.Log("Response: " + responseBody);
        }
    }

    public IEnumerator Borrar(string name)
    {
        string url = "http://localhost:3000/ranking/deleteUser";
        var request = new UnityWebRequest(url, "DELETE");
        string jsonRequestBody = "{\"usuario\":\"" + name + "\"}";
        byte[] bodyRaw = Encoding.UTF8.GetBytes(jsonRequestBody);
        request.uploadHandler = new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");

        yield return request.SendWebRequest();

        if (request.result != UnityWebRequest.Result.Success)
        {
            Debug.LogError("Error sending request: " + request.error);
        }
        else
        {
            Debug.Log("Deleted successful!");
            //SceneManager.LoadScene(1);
            string responseBody = Encoding.UTF8.GetString(request.downloadHandler.data);
            Debug.Log("Response: " + responseBody);
        }
    }
}
