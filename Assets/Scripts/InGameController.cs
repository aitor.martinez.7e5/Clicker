using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InGameController : MonoBehaviour
{
    [SerializeField] Button boost_1;
    [SerializeField] Text score;
    public float weed;
    public float hitPower;
    public float scoreIncreasedPerSecond;
    public float x;
    public GameObject Twerk;
    public GameObject Macarena;
    public GameObject Smoke;

    //Data Server Functs
    DataManager dataManager;

    void Start()
    {
        dataManager = DataManager.instance.GetComponent<DataManager>();
        weed = 0f;
        hitPower = 1f;
        scoreIncreasedPerSecond = 1f;
        x = 0f;
        
    }

    // Update is called once per frame
    void Update()
    {
        score.text = (int)weed + " w";
        Debug.Log(dataManager);
        Debug.Log($"{dataManager.nombreLogedPlayer} , score : {weed}");
        scoreIncreasedPerSecond = x * Time.deltaTime;
        weed = weed + scoreIncreasedPerSecond;
        IncreaseDanceSpeed();
        Submarine();
    }


    public void Hit()
    {
        weed += hitPower;
        dataManager.StartCoroutine(dataManager.Actualizar(dataManager.nombreLogedPlayer, (int)weed));
    }

    public void Boost1Hit()
    {
        weed += 50f;
        dataManager.StartCoroutine(dataManager.Actualizar(dataManager.nombreLogedPlayer, (int)weed));
    }
    public void Boost2Hit()
    {
        x += 1;
        dataManager.StartCoroutine(dataManager.Actualizar(dataManager.nombreLogedPlayer, (int)weed));
    }
    public void DecreaseHit()
    {
        weed -= 20f;
        dataManager.StartCoroutine(dataManager.Actualizar(dataManager.nombreLogedPlayer, (int)weed));
    }
    
    public void IncreaseDanceSpeed()
    {
        Twerk.GetComponent<Animator>().speed = 1 + weed/400f;
        Macarena.GetComponent<Animator>().speed = 1 + weed/400f;
    }
    public void Submarine()
    {
        ParticleSystem.EmissionModule emission = Smoke.GetComponent<ParticleSystem>().emission;
        emission.rateOverTime = weed;
    }













    public void Boost1()
    {
        Button botonPotenciador = GetComponent<Button>();
        //clicsAntesDelPotenciador = clickvalue;
        //clickvalue *= 3;
        botonPotenciador.gameObject.SetActive(false);
    }





}
