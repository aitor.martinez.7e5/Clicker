using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LoginRegisterInputs : MonoBehaviour
{
    public TMP_InputField RegisterUserInputText;
    public TMP_InputField RegisterPassInputText;
    public TMP_InputField LoginUserInputText;
    public TMP_InputField LoginPassInputText;
    public Text Ranking;
    public GameObject apiManager;
    DataManager dataManager;
     
    float rankingTimer = 0;

    void Start()
    {
        dataManager = apiManager.GetComponent<DataManager>();
    }

    void Update()
    {
        rankingTimer -= Time.deltaTime;
        if (rankingTimer < 0)
        {
            rankingTimer = 1;
            ShowRanking();
        }
        
    }

    public void Login()
    {
        dataManager.StartCoroutine(dataManager.Login(LoginUserInputText.text, LoginPassInputText.text));
    }

    public void Register()
    {
        dataManager.StartCoroutine(dataManager.Register(RegisterUserInputText.text, RegisterPassInputText.text));
    }

    public void ShowRanking()
    {
        dataManager.StartCoroutine(dataManager.GetAll());
    }
}
