using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SoundManager : MonoBehaviour
{
    public InGameController score;
    public AudioSource Canutito;
    public AudioSource PIMP;
    public AudioSource Boost1;
    public AudioSource Boost2;
    public AudioSource Malito;

    public AudioSource youngwild;
    //public GameObject ingame;

    // Start is called before the first frame update
    void Start()
    {
        youngwild.Play();

    }

    // Update is called once per frame
    void Update()
    {
        MusicChange();
    }

    public void MusicChange1()
    {
        youngwild.Stop();
        Canutito.Play();
    }

    public void MusicChange()
    {
        if (score.weed >= 1000 && !PIMP.isPlaying)
        {
            Canutito.Stop();
            PIMP.Play();
        }
    }

    public void Boost()
    {

    }
}
